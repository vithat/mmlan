# Introduction


This is the core application repository for a simple APS database.

Two other repos support development, but are *not* needed for live deployment:

* [https://bitbucket.org/erichbschulz/mmla](docker cluster) of node and postgres along with supporting development scripts.
* [https://bitbucket.org/erichbschulz/aps](gulp angular app) supporting linting, liver browser refresh and bulding of the final application front-end javascript and web-assets.

# Deployment intstructions

## 1. Set up configuration

Alter `express/env.js` and `express/public/aps/config.json` as appropriate.


## 2. Activate node

    cd express
    # npm install
    nmp start

## 3. Bootstrap the database (via http)

Use chrome to access `/bootstrap/hardreset`. This will cause `express` to drop and create the session table and `reset` the `sequelize` models.

## 4. Verify success

These two routes are useful:

* `/bootstrap/status`: Express route returning JSON summary results verifying that the tables all exist
* `/aps`: the main front-end app served by express from the static directory (`./express/public/aps/index.html`)
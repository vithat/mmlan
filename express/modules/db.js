var _ = require('lodash');
// var Promise = require('bluebird');

module.exports = function(sequelize) {

var db = {
  run: function(sql, options) {
    return sequelize.query(sql, options);
  },
  get: function(sql, options) {
    return sequelize.query(sql,
      _.extend(
        {type: sequelize.QueryTypes.SELECT}, options));
  },
  patch: function(model_name, id, data) {
    var Model = sequelize.models[model_name];
    data.id = id;
    // load the record to cope with missing attributes:
    var promise = Model.findById(id, {raw: true})
    .then(function (model) {
      _.defaults(data, model);
      return Model.upsert(data);
    })
    .then(function () {
      return Model.findById(id, {paranoid: false});
    }).then(function(model) {
      return model.restore();
    });
    return promise;
  }
};

  return db;
}

"use strict";
var Promise = require('bluebird');
var _ = require('lodash');
var moment = require('moment');

var Aps = function(config) {
  var sys = config.sys;
  // model
  var Patient = sys.models.Patient;
  var Event = sys.models.Event;
  var ReferralRequest = sys.models.ReferralRequest;
  var Appointment = sys.models.Appointment;
  var EpisodeOfCare = sys.models.EpisodeOfCare;
  var fhir_tables = sys.fhir_tables;
  var aps = {};

  function next_round() {
    return moment().tz(config.time_zone)
      // start of tomorow, or today if before 10am
      .subtract(10).startOf('day').add(1, 'day')
      .hour(8);
  }

  /*
   * Receive new data on an APS patient
   * @params object
   * - action string refer|assess|discharge
   * - patient_id integer
   * - user_id integer
   * - data object
   * @returns promise of object
   * - patient object (model)
   * - event object (model)
   */
  aps.receive = function(params) {
    // array of functions to handle specific events
    var actions = {
      refer: recordReferral,
      discharge: recordDischarge,
      assess: recordAssessment
    };
    // get the action method
    var action = actions[params.action];
    if (!action) {
      throw new Error('bad action ' + params.action);
    }
    // package up the event
    var details = params.data;
    var event_rec = {
      user_id: params.user_id,
      patient_id: params.patient_id,
      details: details, // fixme need to strip gumph
    };
    // get patient and save details
    var promise = registerPatient(params).
      then(function (patient) {
        return Promise.props({
          patient: patient,
          event_rec: event_rec
        });
      })
      .then(action);
    return promise;
  };

  /*
   * Get a patient object if in APS
   * @param integer patient_id
   * @returns promise of patient
   */
  aps.getApsRecord = function(patient_id) {
    var result;
    var promises = {};
    // loop over tables and get all the data
    // this is a massive load and will need filtering
    _.forEach(fhir_tables, function(model) {
      promises[model] = sys.models[model].findAll({
        where: {patient_id: patient_id}});
    });
    return Promise.props(_.extend(promises, {
      patient: getPatient(patient_id),
      events: Event.findAll({
        where: {patient_id: patient_id}})
      }))
      .then(function(params) {
        result = params;
        return aps.patientStatus(params);
      })
      .then(function(status) {
        result.status = status;
        return result;
      });
  };

  /*
   * Get a patient object if in APS
   * @param integer patient_id
   * @returns promise of patient
   */
  var getPatient = function(patient_id) {
    return Patient.findOne({where: {id: patient_id}});
  };

  /*
   * Register a patient if not already in APS
   * @params object
   * - patient_id integer
   * - data object
   * @returns promise of patient object
   */
  var registerPatient = function(params) {
    var patient_promise = getPatient(params.patient_id).
    then(function(patient) {
      if (!patient) { // no patient so create
        var new_patient = {
          id: params.patient_id,
          name: params.data.name,
          dob: params.data.dob
          };
        return Patient.create(new_patient);
      }
      else {
        return patient;
      }
    });
    return patient_promise;
  };

  /*
   * Create a referral
   * @params object
   * - patient object (model)
   * - data object (rec)
   * @returns promise of object
   * - patient object (model)
   * - event object (model)
   */
  var recordReferral = function(params) {
    var event_rec = params.event_rec;
    var priority = event_rec.review_plan == 'review today'
      ? 'urgent' : 'routine';
    event_rec.type = 'APS_referral';
    console.log('making referral', params);
    var referral_rec = {
      patient_id: params.patient.id,
      requester: {user: params.user_id},
      status: 'active',
      category: 'request',
      type: 'provide-care',
      specialty: 'Acute-pain',
      description: event_rec.notes,
      priority: priority,
      basedOn: null,
      parent: null,
      context: null,
      fullfillmentStart: null,
      fullfillmentEnd: null,
      reason: null, // codable
      serviceRequested: null, // codable
      supportingInformation: null // references
    };
    var result;
    return recordEvent(params.patient, event_rec)
      .then(function (event_result) {
        // gets patient and event
        result = event_result;
        referral_rec.basedOn = {event: event_result.event.id};
        return ReferralRequest.create(referral_rec);
      })
      .then(function(ReferralRequest) {
        config.log('creating ReferralRequest', ReferralRequest);
        result.referral = ReferralRequest;
        return result;
      });
  };

  /*
   * Register a discharge event
   * @params object
   * - patient object (model)
   * - object (rec)
   * @returns promise of object
   * - patient object (model)
   * - event object (model)
   */
  var recordAssessment = function(params) {
    var event_rec = params.event_rec;
    event_rec.type = 'APS_assessment';
    return recordEvent(params.patient, event_rec);
  };

  /*
   * Register a discharge event
   * @params object
   * - patient object (model)
   * - object (rec)
   * @returns promise of object
   * - patient object (model)
   * - event object (model)
   */
  var recordDischarge = function(params) {
    var event_rec = params.event_rec;
    event_rec.type = 'APS_discharge';
    return recordEvent(params.patient, event_rec);
  };

  /*
   * Create an event
   * @params object
   * - patient object (model)
   * - event(rec)
   * @returns promise of object
   * - patient object (model)
   * - event object (model)
   */
  var recordEvent = function(patient, event_rec) {
    var event_promise = Event.create(event_rec).
    then(function(event) {
      config.log('creating event', event);
      return {
        patient: patient,
        event: event,
      }
    });
    return event_promise;
  };

  /*
   * Summarise the patient status
   * @params
   * - patient object (model)
   * - event object (model) (optional - the event triggering the review)
   * @returns promise of object
   */
  aps.patientStatus = function(params) {
    config.log('getting status params', params);
    var patient = params.patient;
    var promise = Promise.delay(10).
    then(function() {
      var result = {
        age: moment().diff(patient.dob, 'years'),
        dob: patient.dob,
        status: 'poorly',
        event: params.event,
      };
      return result;
    });
    return promise;
  };

  /**
    * Background task runner to find and run unactioned tasks
    *
    * returns immediately
    *
    * # new referrals:
    * - create episodes of care for referral (if none exist)
    * - scedule appointments
    * # ended episode of care:
    * - mark status = 'finished'
    * - update referral request(s) with status='completed'
    */
  aps.poll = function(params) {
    // list all APS reeferrals with accepted status:
    ReferralRequest.findAll({where: {
      status: 'accepted',
      specialty: 'APS'
    }})
    .each(function(referral) {
      console.log('refs', referral.get({plain: true}));
      return makeEpisodeOfCare(referral)
      .then(function(episodeOfCare) {
        console.log('eoc', episodeOfCare.get({plain: true}));
      });
    });
    return Promise.resolve("polling");
  }

  /*
   * Create an EpisodeOfCare from a ReferralRequest
   * @params referral object
   * @returns promise of EpisodeOfCare object
   */
  var makeEpisodeOfCare = function(referral) {
    var result;
    var start = referral.priority == 'urgent'
      ? moment() : next_round();
    var eoc_rec = {
      status: 'active',
      statusHistory: null,
      type: 'APS',
      condition: null,
      start: start,
      end: null,
      referralRequest: [referral.id],
      careManager: null,
      careTeam: null,
      patient_id: referral.patient_id
    };
    return EpisodeOfCare.create(eoc_rec);
  };


  return aps;

}


module.exports = Aps;

//var _ = require('lodash');
var Promise = require('bluebird');

module.exports = function(env) {
  var auth = {};

  auth.hasRole = function(session, role) {
    return session.user && session.user.roles;
  }

  /**
    * Login function
    * @params
    * - session
    * - credentials
    *   - user
    *   - pass
    */
  auth.login = function(session, credentials) {
    return new Promise(function(resolve, reject) {
      if (env.NODE_ENV == 'development') {
        Promise.delay(10).
        then(function() {
          console.log('login by credentials', credentials.user);
          if (credentials.user > 1000 && credentials.pass=='123') {
            user = {
              name: "user " + credentials.user,
              id: credentials.user,
              roles: ["user"],
            };
            console.log('user session created', user);
            session.user = user;
            resolve(user);
          }
          else {
            reject(new Error('failed'));
          }
        });
      }
      else { // production
        reject(new Error('please configure prod authorisation'));
      }
    })
  }

  return auth;
}

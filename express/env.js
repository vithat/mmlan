// environmet configuration variables

module.exports = {
  //  production|development
  NODE_ENV: "development",
  DATABASE_URL: "postgres://postgres:asecret@postgresdb:5432/postgres",
  API_ROUTE: "/api/v1/apsp",
  SESSION_MINUTES: "120",
  COOKIE_SECRET: "thisshouldbesecret",
  PORT: "3000",
  // cors
  // see https://www.npmjs.com/package/cors#configuring-cors-w-dynamic-origin
  // for options to make this accept an array
  CORS_ORIGIN: 'http://localhost:3031',
  TIME_ZONE: 'Australia/Brisbane',
  VERSION: 'experimental', // manually changed
  BUILD_VERSION: '[VERSION]' // set by bamboo during deploy
}

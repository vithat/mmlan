'use strict';
module.exports = function(sequelize, S) {

  var Patient = sequelize.define('Patient', {
    id: {
      type: S.BIGINT,
      primaryKey: true,
      comment: "MRN",
    },
    name: {
      type: S.STRING,
      allowNull: false,
    },
    dob: {
      type: S.DATEONLY,
      allowNull: false,
      validate: {isDate: true},
    },
    location: {
      type: S.STRING,
      allowNull: true,
      comment: "Location of patient with hospital",
    }
  }, {
    tableName: 'patient',
    paranoid: true,
  });

  return Patient;

};

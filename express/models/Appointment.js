"use strict";
module.exports = function(sequelize, S) {
  var Appointment = sequelize.define('Appointment', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    status: {
      type: S.ENUM('proposed', 'pending', 'booked', 'arrived', 'fulfilled',
              'cancelled', 'noshow'),
      allowNull: false,
      defaultValue: 'booked',
      comment: "proposed | pending | booked | arrived | fulfilled | cancelled | noshow"
    },
      //394577000 Anesthetics
      //394882004 Pain management
      // http://hl7.org/fhir/valueset-c80-practice-codes.html
    type: {
      type: S.JSONB,
      allowNull: true,
      comment: "The type of appointment that is being booked",
      defaultValue: {"system" : "http://snomed.info/sct",
        "code" : "394882004",
        "display" : "Pain management"}
    },
    reason: {
      type: S.JSONB,
      allowNull: true,
      comment: "Reason this appointment is scheduled"
    },
    priority: {
      type: S.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 5,
      min: 1,
      max: 9,
      comment: "1 is highest, 9 is lowest"
    },
    description: {
      type: S.TEXT,
      allowNull: true,
      comment: "Shown on a subject line in a meeting request, or appointment list"
    },
    start: {
      type: S.DATE,
      allowNull: true,
      comment: "When appointment is to take place"
    },
    end: {
      type: S.DATE,
      allowNull: true,
      comment: "When appointment is to conclude"
    },
    minutesDuration: {
      type: S.INTEGER.UNSIGNED,
      allowNull: true,
      comment: "Can be less than start/end (e.g. estimate)"
    },
    slot: {
      type: S.JSONB,
      allowNull: true,
      comment: "If provided, then no schedule and start/end values MUST match slot"
    },
    comment: {
      type: S.TEXT,
      allowNull: true,
      comment: "Additional comments"
    },
    participant: {
      type: S.JSONB,
      allowNull: true, // NB user and patient
      comment: "Participants involved in appointment"
    }
  }, {
    tableName: 'appointment',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        Appointment.belongsTo(models.Patient);
        Appointment.belongsTo(models.Event);
      }
    }
  });
  return Appointment;
};

"use strict";
module.exports = function(sequelize, S) {
  var Condition = sequelize.define('Condition', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    encounter: {
      type: S.UUID,
      allowNull: true,
      comment: "Encounter when condition first asserted"
    },
    asserter: {
      type: S.JSONB,
      allowNull: true,
      comment: "Person who asserts this condition"
    },
    code: {
      type: S.JSONB,
      allowNull: false,
      comment: "Identification of the condition, problem or diagnosis"
    },
    category: {
      type: S.JSONB,
      allowNull: true,
      comment: "complaint | symptom | finding | diagnosis"
    },
    clinicalStatus: {
      type: S.ENUM('active', 'relapse', 'remission', 'resolved'),
      allowNull: false,
      comment: "active | relapse | remission | resolved"
    },
    verificationStatus: {
      type: S.ENUM('provisional', 'differential', 'confirmed', 'refuted',
                'entered-in-error', 'unknown'),
      allowNull: false,
      comment: "provisional | differential | confirmed | refuted | entered-in-error | unknown"
    },
    severity: {
      //399166001 fatal
      //24484000 severe
      //6736007 moderate
      //255604002 mild
      type: S.ENUM('mild', 'moderate', 'severe', 'fatal'),
      allowNull: true,
      comment: "Subjective severity of condition"
    },
    onset: {
      type: S.JSONB,
      allowNull: true,
      comment: "Estimated or actual date,  date-time, or age"
    },
    abatement: {
      type: S.JSONB,
      allowNull: true,
      comment: "If/when in resolution/remission"
    },
    stage: {
      type: S.JSONB,
      allowNull: true,
      comment: "Stage/grade, usually assessed formally"
    },
    evidence: {
      type: S.JSONB,
      allowNull: true,
      comment: "Supporting evidence"
    },
    bodySite: {
      type: S.JSONB,
      allowNull: true,
      comment: "Anatomical location, if relevant"
    },
    notes: {
      type: S.TEXT,
      allowNull: true,
      comment: "Additional information about the Condition"
    }
  }, {
    tableName: 'condition',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        Condition.belongsTo(models.Patient);
        Condition.belongsTo(models.Event);
      }
    }
  });
  return Condition;
};

'use strict';
module.exports = function(sequelize, S) {

  var Event = sequelize.define('Event', {
    id: {
      type: S.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    as_at: {
      type: S.DATE,
      defaultValue: S.NOW,
      comment: "The time this event occurred (as opposed to when it was recorded)",
    },
    type: {
      type: S.ENUM(
              'APS_referral', 'APS_discharge', 'APS_assessment'),
      allowNull: false,
    },
    details: {
      type: S.JSONB }
  }, {
    tableName: 'event',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        Event.belongsTo(models.User);
        Event.belongsTo(models.Patient);
      }
    }
  });

  return Event;

};

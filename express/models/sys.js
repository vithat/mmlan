var Promise = require('bluebird');
var moment = require('moment');
var S = require('sequelize');
var fs = require('fs');
var _ = require('lodash');
var readFilePromise = Promise.promisify(fs.readFile);
var files = fs.readdirSync(__dirname);
var path = require('path');
var basename = path.basename(module.filename);

module.exports = function(config) {
  // injected cofiguration variable
  var s = config.sequelize;
  var db = config.db;

  // main export object
  var sys = {};
  // container for our models
  var models = sys.models = {};
  var fhir_tables = sys.fhir_tables = ['ReferralRequest', 'Appointment',
      'EpisodeOfCare', 'Encounter', 'Observation', 'Condition'];
  // auto load all models into sys:
  files.filter(function(file) {
    return (file.slice(-3) === '.js') && (file !== basename);
  })
  .forEach(function(file) {
    var model = s['import'](path.join(__dirname, file));
    console.log('importing model', file, model.name);
    models[model.name] = model;
  });
  Object.keys(models).forEach(function(name) {
    if (models[name].associate) {
      models[name].associate(models);
    }
  });

  /**
    * Return a promise of an object describing overall system status.
    */
  sys.status = function() {
    console.log('getting status');
    var jobs = {
      time: new Date,
      VERSION: config.VERSION,
      BUILD: config.BUILD,
      time_zone: config.time_zone
    };
    _.forEach(_.concat(fhir_tables, ['Event', 'User', 'Patient']), function(model) {
      jobs[model+' count'] = models[model].count();
    });
    return Promise.props(_.extend(jobs, {
      items: db.get("SELECT count(*) AS n FROM items"),
      sessions: db.get("SELECT count(*) AS n FROM session"),
      tables: db.get("SELECT concat_ws('.', schemaname, tablename) as table" +
          " FROM pg_catalog.pg_tables " +
          " WHERE schemaname != 'information_schema' " +
          " AND tablename not like 'pg_%'"),
//      full: db.get("SELECT *" +
//          " FROM pg_catalog.pg_tables WHERE tablename not like 'pg_%'"),
//      user_list: models.User.findAll({attributes: ['name'], raw: true}),
    }));
  };

  function testData() {
    // tomorow at 8am
    var round_starts = moment().tz(config.time_zone).startOf('day').
      add(1, 'day').hour(8);
    return {
      patient: [
        {id: 51, dob: "1962-04-05", name: "Imin Payne"},
        {id: 56, dob: "1962-04-05", name: "Johny Doe"},
        {id: 57, dob: "1962-04-05", name: "John Doey"},
        {id: 12341234, dob: "1962-04-05", name: "John Doe"},
        {id: 2341234, dob: "1962-04-15", name: "Pam Doe"},
      ],
      user: [
        {id: 61231, name: "Gramps"},
        {id: 61234, name: "Bart"},
        {id: 61235, name: "Lisa", role: 'admin'},
      ],
      event: [
        {patient_id: 51, user_id: 61231, type: "APS_referral"},
      ],
      ReferralRequest: {
        specialty: 'APS',
        status: 'accepted',
        request: null,
        recipient: null,
        encounter: null,
        reason: null,
        description: "test data",
        supportingInformation: null,
        fulfillmentTime: null,
      },
      Appointment: {
        status: 'booked',
          //394577000 Anesthetics
          //394882004 Pain management
          // http://hl7.org/fhir/valueset-c80-practice-codes.html
        type: {"system" : "http://snomed.info/sct",
            "code" : "394882004",
            "display" : "Pain management"},
        reason: null,
        priority: 6,
        description: "day 1",
        start: round_starts,
        end: null,
        minutesDuration: null,
        slot: null,
        comment: "routine",
        participant: null
      }
    };
  };

  /**
    * Resets the database for testing purposes only.
    * @return promise
    */
  sys.reset = function(params) {
    var p = _.defaults({}, params);
    var test_data = testData();
    var core; // holds the user, patient, event triparty
    if (config.env === 'development' || p.hard) {
      return models.Event.sync({force: true})
      .then(function() {
        return models.Event.truncate();
      })
      .catch(function() {
        return console.log('failed event sync, continuiing **************');
      })
      .then(function() {
        return models.User.sync({force: true})
          .then(function() {
            return models.User.bulkCreate(test_data.user);
          });
      })
      .then(function() {
        return Promise.props({
          truncate: db.get(
            "TRUNCATE items"),
          reset_counter: db.get(
            "ALTER SEQUENCE items_id_seq RESTART WITH 1"),
          patients: models.Patient.sync({force: true}).then(function() {
            return models.Patient.bulkCreate(test_data.patient);
          }),
        });
      })
      // sycnc the secondary modlels
      .then(function() {
          return fhir_tables;
       })
      .each(function(model) {
        console.log('resetting model', model);
        return models[model].sync({force: true});
      })
      .then(function() {
        console.log('making events');
        return models.Event.bulkCreate(test_data.event,
          {returning: true});
      })
      .then(function(event_res) {
        var event = event_res[0].dataValues;
        var core = _.defaults({
          event_id: event.id,
          }, _.pick(event, ['patient_id', 'user_id']));
        var referral = _.defaults(test_data.ReferralRequest, core);
        console.log('referral', referral);
        return models.ReferralRequest.create(referral);
      })
      .then(function(ref_res) {
        console.log('ref_res', ref_res);
        var appointment = _.defaults(test_data.Appointment, core);
        console.log('appointment', appointment);
        return models.Appointment.create(appointment);
      })
      .then(function(event_res) {
        return sys.status();
      });
    }
    else {
      throw new Error('attempt to reset when not in dev mode');
    }
  };

  sys.upgrade = function() {
    console.log('upgrading');
    return upgradeSql().then(function(sql) {
        return db.run(sql)
        .catch(function(err) {
          if (err.message === 'relation "session" already exists') {
            var msg = 'Session exists';
            console.log(msg);
            return msg;
          }
          else {
            throw err;
          }
        });
    });
  };

  function upgradeSql() {
    var name = path.join(__dirname,
        '../node_modules/connect-pg-simple/table.sql');
    return readFilePromise(name, 'utf-8')
  }

  sys.hardPurge = function() {
    return Promise.mapSeries([
      "DROP TABLE IF EXISTS items",
      "DROP TABLE IF EXISTS session",
      "DROP TABLE IF EXISTS Patients;",
      "DROP TABLE IF EXISTS patients;",
      "DROP TABLE IF EXISTS public.Events;",
      "DROP TABLE IF EXISTS events",
      "DROP TABLE IF EXISTS event",
      "CREATE TABLE items" +
      "(id SERIAL PRIMARY KEY, text VARCHAR(40) not null, complete BOOLEAN)",
      upgradeSql()
    ], function(sql) {
      console.log('running ' + sql);
      return db.run(sql).then(function() {
        return sql;
      });
    });
  }

  return sys;
}

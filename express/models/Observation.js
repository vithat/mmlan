"use strict";
module.exports = function(sequelize, S) {
  var Observation = sequelize.define('Observation', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    status: {
      type: S.ENUM('registered', 'preliminary', 'final', 'amended'),
      allowNull: false,
      comment: "registered | preliminary | final | amended +"
    },
    category: {
      type: S.ENUM('social-history', 'vital-signs', 'imaging', 'laboratory',
              'procedure', 'survey', 'exam', 'therapy'),
      allowNull: false,
      comment: "Classification of  type of observation, http://hl7.org/fhir/valueset-observation-category.html"
    },
    code: {
      type: S.JSONB,
      allowNull: false,
      comment: "Type of observation (code / type)"
    },
    encounter: {
      type: S.UUID,
      allowNull: true,
      comment: "Healthcare event during which this observation is made"
    },
    effective_start: {
      type: S.DATE, defaultValue: S.NOW,
      allowNull: false,
      comment: "Clinically relevant time start for observation"
    },
    effective_end: {
      type: S.DATE, defaultValue: S.NOW,
      allowNull: false,
      comment: "Clinically relevant end for observation"
    },
    issued: {
      type: S.DATE, defaultValue: S.NOW,
      allowNull: false,
      comment: "Date/Time this was made available"
    },
    performer: {
      type: S.JSONB,
      allowNull: true,
      comment: "Who is responsible for the observation"
    },
    value: {
      type: S.JSONB,
      allowNull: true,
      comment: "Actual result"
    },
    dataAbsentReason: {
      type: S.TEXT,
      allowNull: true,
      comment: "not-asked | asked | unkonwn + http://hl7.org/fhir/data-absent-reason"
    },
    interpretation: {
    //  http://hl7.org/fhir/ValueSet/observation-interpretation
    //  http://hl7.org/fhir/v2/0078
    //  A Abnormal
    //  AA  Critically abnormal
    //  B Better
    //  W Worse
    //  D Significant change down
    //  U Significant change up
    //  HH  Critically high
    //  HU  Very high
    //  H High
    //  I Intermediate
    //  L Low
    //  LU  Very low
    //  LL  Critically low
    //  N Normal
    //  IE  Insufficient evidence
    //  IND Indeterminate
      type: S.JSONB,
      allowNull: true,
      comment: "A H L I B W etc"
    },
    comments: {
      type: S.TEXT,
      allowNull: true,
      comment: "Comments about result"
    },
    bodySite: {
      type: S.JSONB,
      allowNull: true,
      comment: "Observed body part"
    },
    method: {
      type: S.JSONB,
      allowNull: true,
      comment: "How it was done"
    },
    specimen: {
      type: S.JSONB,
      allowNull: true,
      comment: "Specimen used for this observation"
    },
    device: {
      type: S.JSONB,
      allowNull: true,
      comment: "(Measurement) Device"
    },
    referenceRange: {
      type: S.JSONB,
      allowNull: true,
      comment: "Provides guide for interpretation"
    },
    related: {
      type: S.JSONB,
      allowNull: true,
      comment: "Resource related to this observation"
    },
    component: {
      type: S.JSONB,
      allowNull: true,
      comment: "Component results"
    }
  }, {
    tableName: 'observation',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        Observation.belongsTo(models.Patient);
        Observation.belongsTo(models.Event);
      }
    }
  });
  return Observation;
};

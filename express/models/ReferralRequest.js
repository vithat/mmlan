"use strict";
module.exports = function(sequelize, S) {
  var ReferralRequest = sequelize.define('ReferralRequest', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    basedOn: {
      type: S.JSONB,
      allowNull: true,
      comment: "ReferralRequest | CarePlan | DiagnosticOrder | ProcedureRequest"
    },
    parent: {
      type: S.UUID,
      allowNull: true,
      comment: "Logical parent in a composite request"
    },
    status: {
      type: S.ENUM(
              'draft', 'accepted', 'active', 'cancelled', 'completed', 'entered-in-error'),
      allowNull: false,
      defaultValue: 'active',
      comment: "j"
    },
    category: {
      type: S.ENUM('proposal', 'plan', 'request'),
      allowNull: false,
      defaultValue: 'request',
      comment: "'requested means authorised"
    },
    type: {
      type: S.ENUM('provide-care', 'assume-management', 'provide-opinion'),
      allowNull: false,
      comment: "Referral/Transition of care request type",
      defaultValue: 'provide-care',
    },
    priority: {
      type: S.ENUM('routine', 'urgent', 'stat', 'asap'),
      allowNull: false,
      defaultValue: 'routine',
      comment: "Urgency of referral / transfer of care request"
    },
    context: {
      type: S.JSONB,
      allowNull: true,
      comment: "Originating encounter or episode of care"
    },
    fullfillmentStart: {
      type: S.DATE,
      allowNull: true,
      comment: "Originating encounter or episode of care"
    },
    fullfillmentEnd: {
      type: S.DATE,
      allowNull: true,
      comment: "Originating encounter or episode of care"
    },
    authored: {
      type: S.DATE, defaultValue: S.NOW,
      allowNull: true,
      comment: "Date of creation/activation"
    },
    requester: {
      type: S.JSONB,
      allowNull: true,
      comment: "Requester of referral / transfer of care"
    },
    specialty: {
      type: S.TEXT,
      allowNull: false,
      comment: "The clinical specialty (discipline) that the referral is requested for"
    },
    recipient: {
      type: S.JSONB,
      allowNull: true,
      comment: "Receiver of referral / transfer of care request"
    },
    reason: {
      type: S.JSONB,
      allowNull: true,
      comment: "Reason for referral / transfer of care request"
    },
    description: {
      type: S.TEXT,
      allowNull: true,
      comment: "A textual description of the referral"
    },
    serviceRequested: {
      type: S.JSONB,
      allowNull: true,
      comment: "Actions requested as part of the referral"
    },
    supportingInformation: {
      type: S.JSONB,
      allowNull: true,
      comment: "Additonal information to support referral or transfer of care request"
    },
  }, {
    tableName: 'referralRequest',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        ReferralRequest.belongsTo(models.User);
        ReferralRequest.belongsTo(models.Patient);
        ReferralRequest.belongsTo(models.Event);
      }
    }
  });
  return ReferralRequest;
};

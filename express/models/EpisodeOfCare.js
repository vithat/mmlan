"use strict";
module.exports = function(sequelize, S) {
  var EpisodeOfCare = sequelize.define('EpisodeOfCare', {
    id: {
      type: S.UUID,
      defaultValue: S.UUIDV1,
      primaryKey: true,
      allowNull: false,
      comment: "Logical id of this artifact"
    },
    status: {
      type: S.ENUM('planned', 'waitlist', 'active', 'onhold', 'finished',
              'cancelled'),
      allowNull: false,
      comment: "planned | waitlist | active | onhold | finished | cancelled"
    },
    statusHistory: {
      type: S.JSONB,
      allowNull: true,
      comment: "Past list of status codes"
    },
    type: {
      type: S.JSONB,
      allowNull: true,
      comment: "Type/class  - e.g. specialist referral, disease management"
    },
    condition: {
      type: S.JSONB,
      allowNull: true,
      comment: "Conditions/problems/diagnoses this episode of care is for"
    },
    start: {
      type: S.DATE,
      allowNull: true,
      comment: "Interval during responsibility is assumed"
    },
    end: {
      type: S.JSONB,
      allowNull: true,
      comment: "Interval during responsibility is assumed"
    },
    referralRequest: {
      type: S.JSONB,
      allowNull: true,
      comment: "Originating Referral Request(s)"
    },
    careManager: {
      type: S.JSONB,
      allowNull: true,
      comment: "Care manager/care co-ordinator for the patient"
    },
    careTeam: {
      type: S.JSONB,
      allowNull: true,
      comment: "Other practitioners facilitating this episode of care"
    }
  }, {
    tableName: 'episodeOfCare',
    paranoid: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        EpisodeOfCare.belongsTo(models.Patient);
      }
    }
  });
  return EpisodeOfCare;
};

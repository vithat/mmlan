module.exports = {
  NODE_ENV: "#{NODE_ENV}", //  production|development
  DATABASE_URL: "#{DATABASE_URL}",
  API_ROUTE: "/api/v1/apsp",
  SESSION_MINUTES: "#{SESSION_MINUTES}",
  COOKIE_SECRET: "#{COOKIE_SECRET}",
  PORT: "#{PORT}",
  // cors
  // see https://www.npmjs.com/package/cors#configuring-cors-w-dynamic-origin
  // for options to make this accept an array
  CORS_ORIGIN: '#{CORS_ORIGIN}',
  TIME_ZONE: 'Australia/Brisbane',
  VERSION: 'experimental', // manually changed by author
  BUILD_VERSION: '[VERSION]' // set by bamboo during deploy
}

// environmet configuration variables
// variables in #curlie format are set by octopus

var router = require('express').Router();

module.exports = function(config) {

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/login', function(req, res) {
  var credentials = req.body;
  console.log('credentials', credentials);
  config.auth.login(req.session, credentials).
  then(function(user) {
    console.log('user', user);
    return res.json({
      success: true,
      name: user.name,
      roles: user.roles
      })
  }).
  catch(function(error) {
    console.log('error', error);
    return res.status(401).json({
      success: false,
      data: 'log in failed' // fixme
    });
  });
});

return router;

}


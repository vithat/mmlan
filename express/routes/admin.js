var router = require('express').Router();
var Promise = require('bluebird');

module.exports = function(config) {
  var sys = config.sys;
  // standard result handlers (promise, req, res)
  var dispatch = config.dispatch;
  // model
  var User = sys.models.User;

  // get the user list:
  router.get('/users', function(req, res) {
    dispatch(User.findAll({
      attributes: ['id', 'name', 'role'],
      raw: true}), req, res);
  });

  router.get('/user/:id', function(req, res) {
    var promise = User.findById(req.params.id);
    dispatch(promise, req, res);
  });

  router.put('/user/:id', function(req, res) {
    // patch model, id, data
    var promise = config.db.patch('User', req.params.id, req.body);
    dispatch(promise, req, res);
  });

  router.delete('/user/:id', function(req, res) {
    var id = req.params.id;
    var promise = User.destroy({where: {id: req.params.id}});
    dispatch(promise, req, res);
  });

  // get the system status
  router.get('/status', function(req, res) {
    sys.status().then(function (stuff) {
      console.log('stuff', stuff);
      return stuff;
    });
    dispatch(sys.status(), req, res);
  });

  // do a hard systme wipe (only for development
  router.get('/reset', function(req, res) {
    var p;
    if (config.env === 'development') {
      p = sys.reset();
    }
    else {
      p = Promise.reject(new Error('cannot reset'));
    };
    dispatch(p, req, res);
  })

  return router;
}

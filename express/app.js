console.log('================ starting MMLA Node App');
'use strict';
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var cors = require('cors');
var _ = require('lodash');

//var assert = require('chai').assert;
//var moment = require('moment');

///////////////////////////////////////////////////////////////////////////////
var env = require('./env');
///////////////////////////////////////////////////////////////////////////////
// db:
console.log('connecting to PostgreSQL using env.DATABASE_URL');
var connectionString = env.DATABASE_URL;
// set up Sequelize
var Sequelize = require('sequelize');
var sequelize = new Sequelize(connectionString);
var db = require('./modules/db')(sequelize);

var auth = require('./modules/auth')(env);
// deprectated pg-promise
//var pgp = require('pg-promise')();
//var db = pgp(connectionString);

///////////////////////////////////////////////////////////////////////////////
// standard success fail methods

var log = function(text, obj) {
  console.log(text, JSON.stringify(obj, null,2));
};

var handle_err = function(err, req, res) {
  console.log('err', err);
  console.log('stack', err.stack);
  return res.status(500).json({success: false, error: err.message});
};

var send = function (data, req, res) {
  var result = {
    success: true,
    data: data,
    counter: req.session.counter,
  }
  return res.json({success: true, data: data});
};

// take a promise and deal with success or failure
var dispatch = function(promise, req, res) {
  req.session.counter = (req.session.counter || 0)+1;
  promise
  .then(function(data) {
    return send(data, req, res)
  })
  .catch(function(err) {handle_err(err, req, res)});
};

///////////////////////////////////////////////////////////////////////////////
// base config object:
var config = {
  VERSION: env.VERSION,
  BUILD: env.BUILD_VERSION,
  time_zone: env.TIME_ZONE,
  db: db,
  auth: auth,
  sequelize: sequelize,
  env: env.NODE_ENV,
  // standard express promise handler for use by routes:
  dispatch: dispatch,
  log: log,
};
var sys = require('./models/sys')(_.defaults({ }, config));
console.log('version:', env.VERSION);

console.log('build:', env.BUILD_VERSION);
sys.upgrade();

///////////////////////////////////////////////////////////////////////////////
// routes:
var api_route = env.API_ROUTE;
console.log('API route:', api_route);
var api = require('./routes/api')(_.defaults({
  sys: sys,
  route: api_route,
  }, config));
var admin = require('./routes/admin')(_.defaults({
  sys: sys,
  route: api_route + '/admin',
  }, config));
var users = require('./routes/users')(config);
var bootstrap = require('./routes/bootstrap')(_.defaults({
  sys: sys,
  }, config));

///////////////////////////////////////////////////////////////////////////////
// app

var app = express();

///////////////////////////////////////////////////////////////////////////////
// session

var pg = require('pg'), pgSession = require('connect-pg-simple')(session);

var minutes = env.SESSION_MINUTES;
var secret = env.COOKIE_SECRET;
console.log('Secret length:', secret.length);
console.log('Session minutes:', minutes);
app.use(session({
  store: new pgSession({
    pg : pg,                                  // Use global pg-module
    conString : env.DATABASE_URL, // Connect using something else than default DATABASE_URL env variable
    tableName : 'session'               // Use another table-name than the default "session" one
  }),
  secret: secret,
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: minutes * 60 * 1000 } // in miliseconds
}));
///////////////////////////////////////////////////////////////////////////////
// security
// see http://stackoverflow.com/questions/9609325/node-js-express-js-user-permission-security-model
function requireRole(role) {
  return function(req, res, next) {
    if(auth.hasRole(req.session, role)) {
      next();
    }
    else {
      console.log('forbidden user', req.session.user);
      res.sendStatus(403);
    }
  }
}
///////////////////////////////////////////////////////////////////////////////
// views
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

///////////////////////////////////////////////////////////////////////////////
// main app

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

console.log('CORS_ORIGIN', env.CORS_ORIGIN);
app.use(cors({
  origin: env.CORS_ORIGIN,
  credentials: true,
  }));
app.use(express.static(path.join(__dirname, 'public')));
// read in routes:
app.use('/user', users); // unnneeded ???
app.use('/', bootstrap);
app.use('/admin', requireRole("admin"), admin);
app.use('/', requireRole("user"), api);

///////////////////////////////////////////////////////////////////////////////
// errors

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

console.log('Env: ', app.get('env'));
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    console.log('in err middleware', err);
    console.log('trace', err.stack);
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  console.log('in err middleware', err);
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
